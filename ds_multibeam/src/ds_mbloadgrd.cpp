//
// Created by ivaughn on 7/11/18.
//

#include <gdal/gdal_priv.h>
#include <gdal/cpl_conv.h>
#include <gdal/cpl_error.h>
#include <ros/ros.h>
#include <string>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <sensor_msgs/PointCloud2.h>

#include <GeographicLib/Geocentric.hpp>
#include <GeographicLib/LocalCartesian.hpp>
#include <GeographicLib/Geodesic.hpp>

#include <ds_multibeam_msgs/MultibeamGrid.h>

int load_map(pcl::PointCloud<pcl::PointXYZ>&cloud, ds_multibeam_msgs::MultibeamGrid& grid) {

  // Load the parameters for lat/lon
  // lat/lon origin:
  std::string nav_ns = ros::param::param<std::string>("~nav_ns", "/nav");
  ROS_INFO_STREAM("Loading nav origin from " <<nav_ns);
  double lat0 = ros::param::param<double>(nav_ns + "/origin_lat", 0);
  double lon0 = ros::param::param<double>(nav_ns + "/origin_lon", 0);
  ROS_INFO_STREAM("Origin: Lat0=" <<lat0 <<" Lon0=" <<lon0);
  GeographicLib::Geocentric earth(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
  GeographicLib::LocalCartesian proj(lat0, lon0, 0, earth);

  // Load the grid via GDAL
  std::string gdalname = ros::param::param<std::string>("~grdname", "");
  if (gdalname.empty()) {
    ROS_FATAL_STREAM("No grid name specified, aborting!");
  }

  ROS_INFO_STREAM("Attempting to load file via GDAL \"" <<gdalname <<"\"");

  GDALDataset* poDataset;
  GDALAllRegister();

  poDataset = (GDALDataset*) GDALOpen(gdalname.c_str(), GA_ReadOnly);
  if (poDataset == NULL) {
    ROS_FATAL_STREAM("Unable to open GDAL file \"" <<gdalname << "\" with:");
    //CPLError();
    return -1;
  }

  ROS_INFO_STREAM("Desc: " <<poDataset->GetDescription());
  ROS_INFO_STREAM("Driver: " <<poDataset->GetDriver()->GetDescription());
  ROS_INFO_STREAM("Raster size: " <<poDataset->GetRasterXSize()
      <<" x " <<poDataset->GetRasterYSize() <<" -> " <<poDataset->GetRasterCount());

  double adfGeoTransform[6];
  const char* proj_ref = poDataset->GetProjectionRef();
  if (proj_ref != NULL && proj_ref[0] != '\0') {
    ROS_ERROR_STREAM("GRD file has a reference frame; it may not be a straight-up lat/lon reference frame");
  }
  if ( poDataset->GetGeoTransform( adfGeoTransform ) == CE_None ) {
    ROS_INFO_STREAM("Tform: Xp = " <<adfGeoTransform[0] <<" + P*" <<adfGeoTransform[1] <<" + L*" <<adfGeoTransform[2]);
    ROS_INFO_STREAM("Tform: Yp = " <<adfGeoTransform[3] <<" + P*" <<adfGeoTransform[4] <<" + L*" <<adfGeoTransform[5]);
  }

  // Get access to the actual data
  GDALRasterBand *poBand;
  poBand = poDataset->GetRasterBand(1);
  GDALDataType dataType = poBand->GetRasterDataType();

  int blocksizeX, blocksizeY;
  poBand->GetBlockSize(&blocksizeX, &blocksizeY);
  int nXSize = poBand->GetXSize();

  ROS_INFO_STREAM("Block size: " <<blocksizeX <<" x " <<blocksizeY);
  ROS_INFO_STREAM("Block datatype: " <<GDALGetDataTypeName(dataType));
  ROS_INFO_STREAM("Band  size: " <<poBand->GetXSize() <<" x " <<poBand->GetYSize());

  // fill in the grid metadata
  grid.cells_easting  = poDataset->GetRasterXSize();
  grid.cells_northing = poDataset->GetRasterYSize();

  // Grab the four corners
  int corner_x[4] = {0, 0, poDataset->GetRasterXSize()-1, poDataset->GetRasterXSize()-1};
  int corner_y[4] = {0, poDataset->GetRasterYSize()-1, poDataset->GetRasterYSize()-1, 0};
  double max_x = std::numeric_limits<double>::quiet_NaN();
  double max_y=max_x, min_x = max_x, min_y = max_y;

  for (size_t i=0; i<4; i++) {
    double x,y,z;

    double lon = adfGeoTransform[0] + corner_x[i]*adfGeoTransform[1] + corner_y[i]*adfGeoTransform[2];
    double lat = adfGeoTransform[3] + corner_x[i]*adfGeoTransform[4] + corner_y[i]*adfGeoTransform[5];
    proj.Forward(lat, lon, 0, x, y, z);

    // comparisons with NaN are ALWAYS false
    // so by using the extra negation we avoid write out the transforms twice
    if (!(min_x < x)) { min_x = x; }
    if (!(min_y < y)) { min_y = y; }

    if (!(max_x > x)) { max_x = x; }
    if (!(max_y > y)) { max_y = y; }
  }

  grid.easting_min  = min_x;
  grid.easting_max  = max_x;
  grid.northing_min = min_y;
  grid.northing_max = max_y;

  grid.nodata_value = std::numeric_limits<float>::quiet_NaN();

  // we have to scan the data twice; once to get the depth offset, once to actually fill the data.
  double min_z = std::numeric_limits<float>::quiet_NaN();
  double max_z = min_z;
  double accum_z = 0;
  double count_z = 0;

  // First, get min/max/average for depth
  void* scanline = CPLMalloc(GDALGetDataTypeSize(dataType) * nXSize );
  for (size_t y=0; y < poDataset->GetRasterYSize(); y++) {
    poBand->RasterIO(GF_Read, 0, y, nXSize, 1, scanline, nXSize, 1, dataType, 0, 0);
    for (size_t x = 0; x < poDataset->GetRasterXSize(); x++) {
      double cell_z;
      if (dataType == GDT_Float32) {
        float z = reinterpret_cast<float *>(scanline)[x];
        if (std::isnan(z)) {
          continue;
        }
        cell_z = static_cast<double>(z);
      } else if (dataType == GDT_Float64) {
        double z = reinterpret_cast<double *>(scanline)[x];
        if (std::isnan(z)) {
          continue;
        }
        cell_z = static_cast<double>(z);
      }

      if (! (min_z < cell_z)) { min_z = cell_z; }
      if (! (max_z > cell_z)) { max_z = cell_z; }
      accum_z += cell_z;
      count_z ++;
    }
  }

  grid.depth_min = min_z;
  grid.depth_max = max_z;
  grid.depth_offset = (accum_z / count_z);

  grid.depth_data.resize(poDataset->GetRasterXSize() * poDataset->GetRasterYSize());

  for (size_t y=0; y < poDataset->GetRasterYSize(); y++) {
    poBand->RasterIO(GF_Read, 0, y, nXSize, 1, scanline, nXSize, 1, dataType, 0, 0);
    for (size_t x = 0; x < poDataset->GetRasterXSize(); x++) {
      double cell_z;
      if (dataType == GDT_Float32) {
        float z = reinterpret_cast<float *>(scanline)[x];
        cell_z = static_cast<double>(z);
      } else if (dataType == GDT_Float64) {
        double z = reinterpret_cast<double *>(scanline)[x];
        cell_z = static_cast<double>(z);
      }
      if (std::isnan(cell_z)) {
        grid.depth_data[y * grid.cells_easting + x] = grid.nodata_value;
      } else {
        grid.depth_data[y * grid.cells_easting + x] = static_cast<float>(cell_z - grid.depth_offset);
      }
    }
  }

  // fill in the pointcloud
  Eigen::Vector3d llz;
  double xyz_x, xyz_y, xyz_z;
  cloud.reserve( poDataset->GetRasterXSize() * poDataset->GetRasterYSize() );

  for (size_t y=0; y < poDataset->GetRasterYSize(); y++) {
    poBand->RasterIO( GF_Read, 0, y, nXSize, 1, scanline, nXSize, 1, dataType, 0, 0);
    for (size_t x=0; x < poDataset->GetRasterXSize(); x++) {
      // Recover lat/lon and depth from the raw map
      llz(0) = adfGeoTransform[0] + x*adfGeoTransform[1] + y*adfGeoTransform[2];
      llz(1) = adfGeoTransform[3] + x*adfGeoTransform[4] + y*adfGeoTransform[5];
      if (dataType == GDT_Float32) {
        llz(2) = reinterpret_cast<float *>(scanline)[x];
      } else if (dataType == GDT_Float64) {
        llz(2) = reinterpret_cast<double *>(scanline)[x];
      }


      // Project lat/lon to xyz
      proj.Forward(llz(1), llz(0), llz(2), xyz_x, xyz_y, xyz_z);

      // add to pointcloud (but only if valid!)
      if (! (std::isnan(xyz_x) || std::isnan(xyz_y) || std::isnan(xyz_z)) ) {
        pcl::PointXYZ pt;
        pt.x = xyz_x;
        pt.y = xyz_y;
        pt.z = xyz_z;
        cloud.points.push_back(pt);
        //std::cout <<"-------------------------------------------------------\n";
        //std::cout <<"(" <<llz(0) <<"," <<llz(1) <<"," <<llz(2) <<")\n";
        //std::cout <<pt <<"\n";
      }
    }
  }
  std::string map_frame_id = ros::param::param<std::string>("~map_frame_id", "odom_dr");
  ros::Time map_time = ros::Time::now();

  cloud.header.frame_id = map_frame_id;
  cloud.header.stamp = pcl_conversions::toPCL(map_time);

  grid.header.frame_id = map_frame_id;
  grid.header.stamp = map_time;

  return 0;
}

int main(int argc, char* argv[] ) {

  ros::init(argc, argv, "mbloadgrd");
  ros::NodeHandle nodeHandle;

  // First, load the map
  pcl::PointCloud<pcl::PointXYZ> cloud;
  ds_multibeam_msgs::MultibeamGrid grid;
  if (load_map(cloud, grid)) {
    return -1;
  }

  ros::Publisher pub_cloud = nodeHandle.advertise<sensor_msgs::PointCloud2>(
      ros::this_node::getName() + "/cloud_grid", 1, true);

  ros::Publisher pub_grid  = nodeHandle.advertise<ds_multibeam_msgs::MultibeamGrid>(
      ros::this_node::getName() + "/grid", 1, true);

  // Now, [possibly periodically] publish it
  double replay_seconds = ros::param::param<double>("~resend_seconds", 0);

  if (replay_seconds == 0) {
    // just send once
    ros::Time map_time = ros::Time::now();
    ROS_INFO_STREAM("Publishing pre-loaded grid as pointcloud...");

    cloud.header.stamp = pcl_conversions::toPCL(map_time);
    pub_cloud.publish(cloud);
    ros::spinOnce();

    grid.header.stamp = map_time;
    pub_grid.publish(grid);
    ros::spinOnce();

    // sleep a little before quitting to be sure messages go out
    ros::Duration(3.0).sleep();

    return 0;
  } else {
    ros::Duration sleep_time(replay_seconds);

    while (ros::ok()) {
      ros::Time map_time = ros::Time::now();

      ROS_INFO_STREAM("Publishing pre-loaded grid as pointcloud...");

      cloud.header.stamp = pcl_conversions::toPCL(map_time);
      pub_cloud.publish(cloud);
      ros::spinOnce();

      grid.header.stamp = map_time;
      pub_grid.publish(grid);
      ros::spinOnce();


      sleep_time.sleep();
    }

  }

  return 0;
}

