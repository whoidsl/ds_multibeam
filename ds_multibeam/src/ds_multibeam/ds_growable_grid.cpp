//
// Created by ivaughn on 7/10/18.
//

#include "ds_multibeam/ds_growable_grid.h"

#include <ds_multibeam_msgs/MultibeamRaw.h>
#include <ds_multibeam_msgs/MultibeamGridStats.h>

namespace dsros {
namespace multibeam {

GrowableGrid::GrowableGrid(double _cellSize) {
  _accum = NULL;
  _weights = NULL;

  _height = 0;
  _width = 0;

  _origin = Eigen::Vector3d::Zero();
  _scale << _cellSize, _cellSize, 1.0;
  _cell_size = _cellSize;

  INIT_SIZE = 500;
  RESIZE_INCREMENT = 200;
  memory_limit_mb = 256;
  memory_used_mb = 0;
}

GrowableGrid::~GrowableGrid() {
  reset();
}

int GrowableGrid::Width() const {
  return _width;
}

int GrowableGrid::Height() const {
  return _height;
}

double GrowableGrid::CellSize() const {
  return _cell_size;
}

double GrowableGrid::TopLeftNorth() const {
  return _origin[0];
}
double GrowableGrid::TopLeftEast() const {
  return _origin[1];
}

void GrowableGrid::setInitSize(int _init_size) {
  INIT_SIZE = _init_size;
}

void GrowableGrid::setResizeIncrement(int _resize_incr) {
  RESIZE_INCREMENT = _resize_incr;
}

void GrowableGrid::setGridSizeLimitMb(size_t mb) {
  memory_limit_mb = mb;
}

void GrowableGrid::reset() {
  if (_accum != NULL) {
    delete[] _accum;
    _accum = NULL;
  }
  if (_weights != NULL) {
    delete[] _weights;
    _weights = NULL;
  }
  _height = 0;
  _width = 0;
  _origin = Eigen::Vector3d::Zero();

  clearStats();
}

void GrowableGrid::clearStats() {
  _stats.soundings_total = 0;
  _stats.soundings_accepted = 0;
  _stats.soundings_flagged = 0;
  _stats.soundings_dropped = 0;

  updateStats();
}

void GrowableGrid::updateStats() {
  _stats.cell_size = _cell_size;
  _stats.width  = _width;
  _stats.height = _height;

  _stats.grid_cells_total = _width * _height;

  _stats.grid_memory_mb = memory_used_mb;
  _stats.grid_memory_limit_mb = memory_limit_mb;

  _stats.grid_origin_easting  = _origin(0);
  _stats.grid_origin_northing = _origin(1);
}

void GrowableGrid::updateCellsUsedStat() {
  size_t used = 0;

  if (_weights == NULL) {
    return;
  }

  for (size_t i=0; i<_height*_width; i++) {
    if (_weights[i] > 0) {
      used++;
    }
  }

  _stats.grid_cells_used = used;
}

const ds_multibeam_msgs::MultibeamGridStats& GrowableGrid::stats() const {
  return _stats;
}

void GrowableGrid::addPing(const MbPing& ping) {
  // add each point to the grid
  for (int i=0; i<ping.Flags().size(); i++) {
    // First, make sure this sounding is usable
    _stats.soundings_total++;
    if (ping.Flags()[i] != ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      _stats.soundings_flagged++;
      continue;
    }

    // THroughout, XYZ will ALWAYS refer to our local/level world (odom_dr, probably), and
    // UVH refers to map grid cell coordinates.  These are only 1 to 1 for a 1.0m grid spacing.

    // Compute the target location
    Eigen::Vector3d out_xyz = ping.Global().points[i].getVector3fMap().cast<double>();
    Eigen::Vector3d out_uvh = xyz2uvh(out_xyz);

    // If we're out of bounds of the current map, resize
    if (out_uvh[0] < 0 || out_uvh[0] >= _width
        || out_uvh[1] < 0 || out_uvh[1] >= _height) {

      if (!resize(out_xyz, out_uvh)) { // send target position in WORLD coordinates...
        // resize can fail, e.g,. if we exceed our limit
        _stats.soundings_dropped++;
        continue;
      }
      // This changes the transform from world to grid coordinates, so we
      // MUST recompute!
      out_uvh = xyz2uvh(out_xyz);
    }

    // Resizing can fail, so check again anyway
    if (!(out_uvh[0] < 0 || out_uvh[0] >= _width
        || out_uvh[1] < 0 || out_uvh[1] >= _height)) {

      // For now, do the stoopidist, simplist
      // possible gridding method
      int rx = round(out_uvh[0]);
      int ry = round(out_uvh[1]);

      // rounding can take something close to the far edge and
      // push it over.  Just push it back..
      rx = (rx == _width ? rx-1 : rx);
      ry = (ry == _height ? ry-1 : ry);

      // Sanity triple-check.  No way these printouts should ever happen
      if (!(rx >= 0 && rx < _width
          && ry >= 0 && ry < _height)) {
        std::cout <<"RX: " <<rx <<"  WIDTH: " << _width <<std::endl;
        std::cout <<"RY: " <<ry <<" HEIGHT: " <<_height <<std::endl;
        std::cout <<" U: " <<out_uvh[0] <<" V: " <<out_uvh[1] <<std::endl <<std::endl;
      }

      // Also assert that we won't buffer overflow
      assert(rx >= 0);
      assert(rx < _width);
      assert(ry >= 0);
      assert(ry < _height);
      assert(rx + ry*_width < _width*_height);
      _accum[rx + ry*_width] += out_uvh[2];
      _weights[rx + ry*_width] += 1;
      _stats.soundings_accepted++;
    } else {
      _stats.soundings_dropped++;
    }
  } // for each sounding in ping

  // update our stats for reporting
  updateStats();
  updateCellsUsedStat();
}

bool GrowableGrid::resize(const Eigen::Vector3d& tgt_xyz, const Eigen::Vector3d& tgt_uvh) {
  // first, see if we have a grid at _all_
  if (_accum == NULL || _weights == NULL) {
    // do the initial allocation, and put our target point at the center
    _height = INIT_SIZE;
    _width = INIT_SIZE;
    _accum = new double[_height*_width];
    _weights = new double[_height*_width];
    for (int i=0; i<_height*_width; i++) {
      _accum[i]=0;
      _weights[i]=0;
    }

    // set the origin for future points
    _origin[0] = tgt_xyz[0] - _width*_cell_size/2.0;
    _origin[1] = tgt_xyz[1] - _height*_cell_size/2.0;
    std::cout <<"Resizing to " <<_width <<" x " <<_height <<" grid cells" <<std::endl;
    memory_used_mb = (2*sizeof(double)*_height*_width)/(1024*1024);
    updateStats();
    updateCellsUsedStat();
    return true;
  }

  // we already have a grid, but it isn't big enough
  int locX = 0, locY = 0; // position of current data in new grid
  int newHeight = _height, newWidth=_width;

  // handle each axis seperately
  // x
  if (tgt_uvh[0] < 0) {
    int toAdd = round(fabs(tgt_uvh[0])) + RESIZE_INCREMENT;
    newWidth += toAdd;
    locX = toAdd;
    _origin[0] -= (locX * _cell_size);
  } else if (tgt_uvh[0] > _width) {
    newWidth += (round(tgt_uvh[0]) - _width + RESIZE_INCREMENT);
  }

  // y
  if (tgt_uvh[1] < 0) {
    int toAdd = round(fabs(tgt_uvh[1])) + RESIZE_INCREMENT;
    newHeight += toAdd;
    locY = toAdd;
    _origin[1] -= (locY * _cell_size);

  } else if (tgt_uvh[1] > _height) {
    newHeight += (round(tgt_uvh[1]) - _height + RESIZE_INCREMENT);
  }

  double new_size_mb = (2*sizeof(double)*_height*_width)/(1024*1024);
  if (new_size_mb > memory_limit_mb) {
    std::cerr <<"New grid requires " <<new_size_mb <<"MB, but limit is only " <<memory_limit_mb <<"MB!  Dropping...";
    return false;
  }

  std::cout <<"Resizing to " <<newWidth <<" x " << newHeight <<" grid cells" <<std::endl;
  // do the resize
  double* newAccum = new double[newHeight*newWidth];
  double* newWeights =  new double[newHeight*newWidth];
  // initialize
  memset(newAccum, 0, sizeof(double)*newHeight*newWidth);
  memset(newWeights, 0, sizeof(double)*newHeight*newWidth);

  // copy the old data into the new data
  for (int y=0; y<_height; y++) {
    for (int x=0; x<_width; x++) {
      newAccum[(x+locX) + (y+locY)*newWidth] = _accum[x + y*_width];
      newWeights[(x+locX) + (y+locY)*newWidth] = _weights[x + y*_width];
    }
  }


  // delete the old data and swap
  delete[] _accum;
  delete[] _weights;

  _accum = newAccum;
  _weights = newWeights;
  _width = newWidth;
  _height = newHeight;
  memory_used_mb = new_size_mb;

  // update our statistics
  updateStats();
  updateCellsUsedStat();
}

}
}