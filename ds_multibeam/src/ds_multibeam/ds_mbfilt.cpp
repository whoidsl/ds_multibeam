//
// Created by ivaughn on 7/9/18.
//

#include <ros/ros.h>
#include "ds_multibeam/ds_mbfilt.h"

namespace dsros {
namespace multibeam {

DsMbFilt::DsMbFilt() {
  clearStats();
}

void DsMbFilt::setupParameters() {
  // default parameters turn filters off
  filter_min_altitude = ros::param::param<double>(ros::this_node::getName() + "/filter/min_altitude", 0.0);
  filter_max_altitude = ros::param::param<double>(ros::this_node::getName() + "/filter/max_altitude", 15000.0);
  filter_max_altitude_jump = ros::param::param<double>(ros::this_node::getName() + "/filter/max_altitude_jump", 15000.0);

  filter_min_range = ros::param::param<double>(ros::this_node::getName() + "/filter/min_range", 0.0);
  filter_max_range = ros::param::param<double>(ros::this_node::getName() + "/filter/max_range", 15000.0);
  filter_max_range_jump = ros::param::param<double>(ros::this_node::getName() + "/filter/max_range_jump", 15000.0);

  filter_min_depth = ros::param::param<double>(ros::this_node::getName() + "/filter/min_depth", 0.0);
  filter_max_depth = ros::param::param<double>(ros::this_node::getName() + "/filter/max_depth", 15000.0);
  filter_max_depth_jump = ros::param::param<double>(ros::this_node::getName() + "/filter/max_depth_jump", 15000.0);

  filter_backup_dist = ros::param::param<double>(ros::this_node::getName() + "/filter/backup_dist", 10000.0);
  filter_max_range_hist_frac = ros::param::param<double>(ros::this_node::getName() + "/filter/max_range_hist_frac", 10000.0);
}

void DsMbFilt::clearStats() {
  _stats.soundings = 0;
  _stats.accepted = 0;

  _stats.preflagged = 0;
  _stats.range_condition = 0;
  _stats.altitude_condition = 0;
  _stats.depth_condition = 0;
  _stats.backup_condition = 0;
}

const ds_multibeam_msgs::MultibeamFilterStats& DsMbFilt::stats() const {
  return _stats;
}

void DsMbFilt::filter(const MbPing& input, MbPing& output) {
  // Copy the ping
  output = input;

  // Run the inner filter function from the middle out.  Twice.
  int center = input.Flags().size();

  // first, center to right
  _filter_inner(output, center, 1);

  // next, center to left
  _filter_inner(output, center-1, -1);

  /*
  std::cout <<"\tFILTER: " <<_stats.accepted <<" of " <<_stats.soundings <<"\n";
  std::cout <<"\t\tPRE  : " <<_stats.preflagged <<"\n";
  std::cout <<"\t\tALT  : " <<_stats.altitude_condition <<"\n";
  std::cout <<"\t\tRANGE: " <<_stats.range_condition <<"\n";
  std::cout <<"\t\tDEPTH: " <<_stats.depth_condition <<"\n";
  std::cout <<"\t\tBACK : " <<_stats.backup_condition <<std::endl;
  size_t accum = 0;
  for (size_t i=0; i<output.Flags().size(); i++) {
    if (output.Flags()[i] == ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      accum ++;
    }
  }

  std::cout <<"\tPOST-FILTER: " <<accum <<" of " <<output.Flags().size();
   */
}

void DsMbFilt::_filter_inner(MbPing& ret, int start, int delta) {

  std::vector<uint8_t>& flags = ret.Flags();
  int lastGoodPing = -1;
  double lastGoodRange = -1;
  double lastGoodAltitude = -1;
  double lastGoodDepth = -1;
  double lastGoodAbsY = -1;
  for (int i=start; i>=0 && i<flags.size(); i+=delta) {
    _stats.soundings++;

    // Check if already flagged by multibeam
    if ( flags[i] != ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      _stats.preflagged++;
      continue;
    }

    // Range constraints
    double range = ret.Local().points[i].getArray3fMap().matrix().norm();
    if (range < filter_min_range || range > filter_max_range
      || (lastGoodPing >= 0 && fabs(range - lastGoodRange) > filter_max_range_jump)) {

      flags[i] = ds_multibeam_msgs::MultibeamRaw::BEAM_BAD_FILTER;
      _stats.range_condition++;
      continue;
    }

    // altitude constraints
    double altitude = fabs(ret.Local().points[i].z);
    if (altitude < filter_min_altitude || altitude > filter_max_altitude
      || (lastGoodPing >= 0 && fabs(altitude - lastGoodAltitude) > filter_max_altitude_jump)) {

      flags[i] = ds_multibeam_msgs::MultibeamRaw::BEAM_BAD_FILTER;
      _stats.altitude_condition++;
      continue;
    }

    // Range constraints
    double depth = fabs(ret.Global().points[i].z);
    if (depth < filter_min_depth || depth > filter_max_depth
      || (lastGoodPing >= 0 && fabs(depth - lastGoodDepth) > filter_max_depth_jump)) {

      flags[i] = ds_multibeam_msgs::MultibeamRaw::BEAM_BAD_FILTER;
      _stats.depth_condition++;
      continue;
    }

    // Dana's backup metric
    // this loop ALWAYS runs from center towards outer beams.  Thus, the current y-coordinate (across-track)
    // should have a greater magnitude than the last good ping: that is, abs(y_now) - abs(y_lastGood)) >= 0.
    // However, we allow a small tolerance
    double absY = ret.Local().points[i].y;
    if (lastGoodPing >= 0 && (absY - lastGoodAbsY) < -filter_backup_dist) {
      flags[i] = ds_multibeam_msgs::MultibeamRaw::BEAM_BAD_FILTER;
      _stats.backup_condition ++;
      continue;
    }

    // Histogram thingy
    // TODO: Not implemented

    // Ok, no filter has yet rejected this, so we accept it!
    _stats.accepted++;

    lastGoodPing = i;
    lastGoodRange = range;
    lastGoodAltitude = altitude;
    lastGoodDepth = depth;
    lastGoodAbsY = absY;
  }

}

}
}
