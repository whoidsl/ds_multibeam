//
// Created by ivaughn on 7/9/18.
//

#include "ds_multibeam/ds_mbping.h"
#include <ds_multibeam_msgs/MultibeamRaw.h>
#include <pcl/common/transforms.h>
#include <limits>

namespace dsros {
namespace multibeam {

MbPing::MbPing() {
  reset();
}

void MbPing::setLocalData(const std::vector<uint8_t>& _beamflag, const PointCloud& _local) {
  beamflag = _beamflag;
  local = _local;
}

void MbPing::applyNav(const Eigen::Affine3d& nav) {
  Eigen::Affine3f nav_float = nav.cast<float>();

  pcl::transformPointCloud(local, global, nav_float);
}

void MbPing::reset() {
  beamflag.resize(0);
  global.resize(0);
  local.resize(0);
}

const PointCloud & MbPing::Global() const {
  return global;
}

const PointCloud & MbPing::Local() const {
  return local;
}

const std::vector<uint8_t> & MbPing::Flags() const {
  return beamflag;
}

std::vector<uint8_t> & MbPing::Flags() {
  return beamflag;
}

// TODO: Decide if this should be eliminated
void MbPing::updateFromFlags() {
  for (size_t i=0; i<beamflag.size(); i++) {
    if (beamflag[i] != ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      if (local.points.size() == beamflag.size()) {
        local.points[i].x = std::numeric_limits<float>::quiet_NaN();
        local.points[i].y = std::numeric_limits<float>::quiet_NaN();
        local.points[i].z = std::numeric_limits<float>::quiet_NaN();
      }
      if (global.points.size() == beamflag.size()) {
        global.points[i].x = std::numeric_limits<float>::quiet_NaN();
        global.points[i].y = std::numeric_limits<float>::quiet_NaN();
        global.points[i].z = std::numeric_limits<float>::quiet_NaN();
      }
    }
  }

  local.is_dense = false;
  global.is_dense = false;

}

// strip out bad data
void MbPing::GlobalDense(PointCloud &ret) const {
  ret.reserve(global.points.size());

  for (size_t i=0; i<global.points.size(); i++) {
    if (beamflag[i] == ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      ret.points.push_back(global.points[i]);
    }
  }

  ret.header = global.header;
  ret.width = ret.points.size();
  ret.height = 1;
  ret.is_dense = true;
}

void MbPing::LocalDense(PointCloud &ret) const {
  ret.reserve(local.points.size());

  for (size_t i=0; i<local.points.size(); i++) {
    if (beamflag[i] == ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      ret.points.push_back(local.points[i]);
    }
  }

  ret.header = global.header;
  ret.width = ret.points.size();
  ret.height = 1;
  ret.is_dense = true;
}

}
}