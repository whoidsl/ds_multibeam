//
// Created by ivaughn on 7/6/18.
//


#include "ds_multibeam/ds_mbproc_live.h"

#include <ds_multibeam_msgs/MultibeamRaw.h>
#include <ds_multibeam_msgs/MultibeamGrid.h>
#include <ds_multibeam_msgs/MultibeamFilterStats.h>
#include <ds_multibeam_msgs/MultibeamGridStats.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <visualization_msgs/Marker.h>
#include <pcl_ros/point_cloud.h>
#include <tf2_eigen/tf2_eigen.h>

#include <ds_multibeam/ds_mbping.h>

namespace dsros {
namespace multibeam {

DsMbprocLive::DsMbprocLive() {
  // so far so good
}

DsMbprocLive::DsMbprocLive(int argc, char *argv[], const std::string &name)
    : ds_base::DsProcess(argc, argv, name) {
  // do nothing else
}

DsMbprocLive::~DsMbprocLive() {
  // do nothing special
}

void DsMbprocLive::setupParameters() {

  pointcloud_reset_interval = ros::Duration(ros::param::param<double>("~pointcloud_reset_interval_sec", 60));
  grid_update_interval = ros::Duration(ros::param::param<double>("~grid_update_interval_sec", 60));

  map_frame_id = ros::param::param<std::string>("~map_frame_id", "odom_dr");
  raw_multibeam_topic =
      ros::param::param<std::string>("~raw_multibeam_topic", ros::this_node::getName() + "/raw_multibeam");

  mb_filter.setupParameters();
  mb_gridder.setupParameters();
}
void DsMbprocLive::setupSubscriptions() {
  auto nh = nodeHandle();

  // TF input
  tf_listener_.reset(new tf2_ros::TransformListener(tfBuffer_, nh));

  // raw input data
  multibeam_raw =
      nh.subscribe(raw_multibeam_topic, 3, &DsMbprocLive::_handleMultibeamMsg, this);
}
void DsMbprocLive::setupPublishers() {
  auto nh = nodeHandle();

  pointcloud_raw = nh.advertise<PointCloud>(ros::this_node::getName() + "/cloud_raw", 3, false);
  pointcloud_filt = nh.advertise<sensor_msgs::PointCloud2>(ros::this_node::getName() + "/cloud_filt", 3, false);
  pointcloud_grid = nh.advertise<PointCloud>(ros::this_node::getName() + "/cloud_grid", 3, false);

  pub_stats_filter  = nh.advertise<ds_multibeam_msgs::MultibeamFilterStats>(ros::this_node::getName() + "/stats/filter", 5);
  pub_stats_gridder = nh.advertise<ds_multibeam_msgs::MultibeamGridStats>(ros::this_node::getName() + "/stats/grid", 5);

  grid_pub = nh.advertise<ds_multibeam_msgs::MultibeamGrid>(ros::this_node::getName() + "/grid", 3, true);
}

void DsMbprocLive::_handleMultibeamMsg(const ds_multibeam_msgs::MultibeamRaw &msg) {

  // Sanity check
  if ( msg.beamflag.size() != msg.twowayTravelTime.size()
      ||  msg.beamflag.size() != msg.angleAlongTrack.size()
      ||  msg.beamflag.size() != msg.angleAcrossTrack.size()) {
    ROS_ERROR_STREAM("Got multibeam message where twowaytraveltime, alongtrack, across track, and beamflag do not have all the same size; dropping message");
    return;
  }

  // compute our raw points
  ros::Time ping_time;
  MbPing raw_ping;
  MbPing filt_ping;
  DsMbprocLive::PointCloud raw_local_cloud, global_raw_cloud, global_filt_cloud;

  // Build the ping
  try {
    ping_time = _toPointcloudSimple(raw_local_cloud, msg);

    geometry_msgs::TransformStamped nav_raw_tform;
    nav_raw_tform = tfBuffer_.lookupTransform(map_frame_id, msg.header.frame_id, ping_time);
    Eigen::Affine3d nav_tform = tf2::transformToEigen(nav_raw_tform);

    raw_ping.setLocalData(msg.beamflag, raw_local_cloud);
    raw_ping.applyNav(nav_tform);

  } catch (const tf2::TransformException& exception) {
    // one or two exceptions is not uncommon, if it recurrs we could have issues
    ROS_ERROR_STREAM("Unable to get transform for ping! Dropping.  Error message: \n" <<exception.what());
    return;
  }

  // Run some filtering
  mb_filter.filter(raw_ping, filt_ping);

  // decide if we're updating our pointcloud, resetting it, whatever
  if (raw_accum.size() == 0 || ping_time - pointcloud_start > pointcloud_reset_interval) {
    raw_accum.resize(0);
    filt_accum.resize(0);
    pointcloud_start = ping_time;
  }

  // fill in the output pointcloud
  raw_ping.GlobalDense(global_raw_cloud);
  filt_ping.GlobalDense(global_filt_cloud);
  raw_accum += global_raw_cloud;
  filt_accum += global_filt_cloud;

  raw_accum.header.frame_id = map_frame_id;
  raw_accum.header.stamp = pcl_conversions::toPCL(ping_time);
  filt_accum.header = raw_accum.header;

  // Grid
  mb_gridder.addPing(filt_ping);

  // Publish updating pointcloud
  pointcloud_raw.publish(raw_accum);
  pointcloud_filt.publish(filt_accum);

  // publish our stats
  pub_stats_filter.publish(mb_filter.stats());
  pub_stats_gridder.publish(mb_gridder.stats());

  // Publish updated grid
  if (ping_time - grid_last_update > grid_update_interval) {
    grid_last_update = ping_time;

    PointCloud grid_cloud;
    mb_gridder.fillOutput(grid_cloud);
    grid_cloud.header.frame_id = map_frame_id;
    grid_cloud.header.stamp = pcl_conversions::toPCL(ping_time);
    pointcloud_grid.publish(grid_cloud);

    ds_multibeam_msgs::MultibeamGrid grid_msg;
    mb_gridder.fillOutput(grid_msg);
    grid_msg.header.frame_id = map_frame_id;
    grid_msg.header.stamp = ping_time;
    grid_pub.publish(grid_msg);
  }
}

ros::Time DsMbprocLive::_toPointcloudSimple(DsMbprocLive::PointCloud& ret,
                           const ds_multibeam_msgs::MultibeamRaw& msg) {

  // This is the SIMPLE version of the pointcloud math.  It assumes we can use vehicle orientation at the
  // average time of TX and RX.  This is a decent assumption until approximately 100m altitude.  So... ish?



  // Let's find the average one-way travel time
  double traveltime_accum = 0;
  int traveltime_denom = 0;
  for (size_t i=0; i<msg.beamflag.size(); i++) {
    if (msg.beamflag[i] == ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      traveltime_accum += (msg.twowayTravelTime[i]/2.0);
      traveltime_denom++;
    }
  }
  double average_time = traveltime_accum / static_cast<double>(traveltime_denom);
  ros::Time ping_time = msg.header.stamp + ros::Duration(average_time);

  // based on that, let's get our transform at the average of RX and TX (this is WRONG, TODO)
  ROS_INFO_STREAM("PING! avg time: " <<average_time);


  geometry_msgs::TransformStamped avg_raw_tform;
  try {
    avg_raw_tform = tfBuffer_.lookupTransform(map_frame_id, msg.header.frame_id, ping_time);
  } catch (const tf2::TransformException& exception) {
    // one or two exceptions is not uncommon, if it recurrs we could have issues
    ROS_ERROR_STREAM("Unable to get transform for ping! Dropping.  Error message: \n" <<exception.what());
    return ping_time;
  }
  Eigen::Affine3d avg_tform = tf2::transformToEigen(avg_raw_tform);

  // compute our raw points
  DsMbprocLive::PointCloud raw_cloud;

  // there was an old bug in PCL that required resizing the points array directly.  So we still dot hat.
  ret.resize(msg.beamflag.size());
  ret.points.resize(msg.beamflag.size());

  Eigen::Vector3d pt;
  pt << 0, 0, 1.0;
  for (size_t i=0; i<msg.beamflag.size(); i++) {
    Eigen::Quaterniond beam_tform = Eigen::AngleAxisd(msg.angleAcrossTrack[i], Eigen::Vector3d(1,0,0))
                               * Eigen::AngleAxisd(msg.angleAlongTrack[i],  Eigen::Vector3d(0,1,0));
    Eigen::Vector3d tmp = ((msg.twowayTravelTime[i]*msg.soundspeed/2.0)*(beam_tform*pt));
    tmp(2) *= -1.0;
    //Eigen::Vector3d sounding = avg_tform * tmp;
    //ret.points[i].getVector3fMap() = sounding.cast<float>();
    ret.points[i].getVector3fMap() = tmp.cast<float>();
  }
  ret.width=msg.beamflag.size();
  ret.height=1;

  return ping_time;
}

/*
 *
 * TODO
void DsMbprocLive::_toPointcloudFull(DsMbprocLive::PointCloud& ret,

                                       const ds_multibeam_msgs::MultibeamRaw& msg) {
  // This is the COMPLETE version of this function. It uses the stuff in
  // Beaudoin, Clarke and Bartlet, "Application of surface sound speed measurements in
  // Post-processing for multi-sector multibeam echo sounders"



  // Let's find the average one-way travel time
  double traveltime_accum = 0;
  int traveltime_denom = 0;
  for (size_t i=0; i<msg.beamflag.size(); i++) {
    if (msg.beamflag[i] == ds_multibeam_msgs::MultibeamRaw::BEAM_OK) {
      traveltime_accum += msg.twowayTravelTime[i];
      traveltime_denom++;
    }
  }
  double average_time = traveltime_accum / static_cast<double>(traveltime_denom);
  ros::Time ping_time = msg.header.stamp + ros::Duration(average_time);

  // based on that, let's get our transform at the average of RX and TX (this is WRONG, TODO)
  ROS_INFO_STREAM("PING! avg time: " <<average_time);

  geometry_msgs::TransformStamped avg_raw_tform;
  try {
    avg_raw_tform = tfBuffer_.lookupTransform(map_frame_id, msg.header.frame_id, ping_time);
  } catch (const tf2::TransformException& exception) {
    // one or two exceptions is not uncommon, if it recurrs we could have issues
    ROS_ERROR_STREAM("Unable to get transform for ping! Dropping.  Error message: \n" <<exception.what());
    return;
  }
  Eigen::Affine3d avg_tform = tf2::transformToEigen(avg_raw_tform);

  // compute our raw points
  DsMbprocLive::PointCloud raw_cloud;

  // there was an old bug in PCL that required resizing the points array directly.  So we still dot hat.
  ret.resize(msg.beamflag.size());
  ret.points.resize(msg.beamflag.size());

  Eigen::Vector3d pt;
  pt << 0, 0, 1.0;
  for (size_t i=0; i<msg.beamflag.size(); i++) {
    Eigen::Quaterniond beam_tform = Eigen::AngleAxisd(msg.angleAcrossTrack[i], Eigen::Vector3d(1,0,0))
                               * Eigen::AngleAxisd(msg.angleAlongTrack[i],  Eigen::Vector3d(0,1,0));
    Eigen::Vector3d tmp = ((msg.twowayTravelTime[i]*msg.soundspeed)*(beam_tform*pt));
    tmp(2) *= -1.0;
    Eigen::Vector3d sounding = avg_tform * tmp;
    ret.points[i].getVector3fMap() = sounding.cast<float>();
  }
  ret.width=msg.beamflag.size();
  ret.height=1;
}
}
 */
void DsMbprocLive::_filterPointcloud(PointCloud& ret, const PointCloud& inp) {
  int center = inp.size()/2;



}

void DsMbprocLive::_cookPointcloud(PointCloud& ret, const PointCloud& inp) {

}

}
}