//
// Created by ivaughn on 7/6/18.
//

#ifndef PROJECT_DS_MBPROC_LIVE_H
#define PROJECT_DS_MBPROC_LIVE_H

#include <ds_base/ds_process.h>
#include <tf2_ros/transform_listener.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <ds_multibeam_msgs/MultibeamRaw.h>
#include <Eigen/Dense>

#include "ds_mbfilt.h"
#include "ds_mbgrid.h"

namespace dsros {
namespace multibeam {

class DsMbprocLive : public ds_base::DsProcess {

public:
  typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
  explicit DsMbprocLive();
  DsMbprocLive(int argc, char* argv[], const std::string& name);

  ~DsMbprocLive() override;

  DS_DISABLE_COPY(DsMbprocLive);

 protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;

  void _handleMultibeamMsg(const ds_multibeam_msgs::MultibeamRaw& msg);
  ros::Time _toPointcloudSimple(PointCloud& ret, const ds_multibeam_msgs::MultibeamRaw& msg);
  //void _toPointcloudFull(PointCloud& ret, const ds_multibeam_msgs::MultibeamRaw& msg);

  void _filterPointcloud(PointCloud& ret, const PointCloud& inp);
  void _cookPointcloud(PointCloud& ret, const PointCloud& inp);

 private:
  // ROS I/O stuff
  ros::Publisher pointcloud_raw;
  ros::Publisher pointcloud_filt;
  ros::Publisher pointcloud_grid;
  ros::Publisher grid_pub;
  ros::Publisher pub_stats_filter;
  ros::Publisher pub_stats_gridder;

  ros::Subscriber multibeam_raw;

  // TF2 listeners
  std::unique_ptr<tf2_ros::TransformListener> tf_listener_;
  tf2_ros::Buffer tfBuffer_;

  // parameters we need to load
  std::string raw_multibeam_topic;
  std::string map_frame_id;

  // stuff to decide when to crop the pointcloud and re-publish the map
  ros::Duration pointcloud_reset_interval;
  ros::Duration grid_update_interval;

  // accumulated data
  ros::Time pointcloud_start;
  PointCloud raw_accum;
  PointCloud filt_accum;

  ros::Time grid_last_update;

  // sub processing steps
  DsMbFilt mb_filter;
  DsMbGrid mb_gridder;
};

}
}

#endif //PROJECT_DS_MBPROC_LIVE_H
