//
// Created by ivaughn on 7/9/18.
//

#ifndef PROJECT_MBPING_H
#define PROJECT_MBPING_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <ds_multibeam_msgs/MultibeamRaw.h>

namespace dsros {
namespace multibeam {

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

class MbPing {

 public:
  MbPing();
  void reset();

  void setLocalData(const std::vector<uint8_t>& beamflag, const PointCloud& local);
  void applyNav(const Eigen::Affine3d& nav);

  // get the data in a cloud with bad data having all NaNs
  const PointCloud& Global() const;
  const PointCloud& Local() const;

  const std::vector<uint8_t>& Flags() const;
  std::vector<uint8_t>& Flags();

  // update the bad flags from the flags vector
  void updateFromFlags();

  // strip out bad data
  void GlobalDense(PointCloud& ret) const;
  void LocalDense(PointCloud& ret) const;

 protected:
  std::vector<uint8_t> beamflag;
  PointCloud global;
  PointCloud local;

};

}
}
#endif //PROJECT_MBPING_H
